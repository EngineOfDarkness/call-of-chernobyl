<?xml version="1.0" encoding="windows-1251" ?>
<string_table>

	<string id="ls_header">
		<text>100 ZONE SURVIVAL TIPS</text>
	</string>
	
	<string id="ls_tip_number">
		<text>Tip #</text>
	</string>
	
	<string id="ls_tip_1">
		<text>Visit the Team EPIC forums at www.epicstalker.com for news, information and other media.</text>
	</string>

	<string id="ls_tip_2">
		<text>If items are weighing you down and you don't want to sell them, you can use a backpack to create a stash. Removing all items from a stash will return the backpack to your inventory.</text>
	</string>

	<string id="ls_tip_3">
		<text>The weight you are carrying reduces your endurance. Excess weight will restrict your mobility, while being completely overloaded will stop you from moving altogether.</text>
	</string>

	<string id="ls_tip_4">
		<text>You can hide and show different markers on the map in your PDA using filters. Filter buttons are located above the map, under the current mission line.</text>
	</string>

	<string id="ls_tip_5">
		<text>Some suits have integrated helmets. It is not possible to use other helmets with such suits.</text>
	</string>

	<string id="ls_tip_6">
		<text>To wait out an emission, take cover in a reliable building or underground. When an emission is approaching, the nearest cover will be marked on your PDA.</text>
	</string>

	<string id="ls_tip_7">
		<text>Weapon flatness affects bullet trajectory, while handling determines the time it takes the sight to return to its original position after firing.</text>
	</string>

	<string id="ls_tip_8">
		<text>You can use a bandage, a military medkit or Vinca drugs to stop bleeding. If you don't deal with it quickly, bleeding can seriously damage your health.</text>
	</string>

	<string id="ls_tip_9">
		<text>Drugs don't take effect immediately and some last for quite a long time.</text>
	</string>

	<string id="ls_tip_10">
		<text>You can access detailed information about the current mission by holding "$$ACTION_SCORES$$".</text>
	</string>

	<string id="ls_tip_11">
		<text>You can unload the weapons you find to get more ammo. To do this, right-click on the weapon in your backpack and select the appropriate action from the context menu.</text>
	</string>

	<string id="ls_tip_12">
		<text>To use an item via the quick access slot, drag it from your backpack to one of the four slots above the artefact containers.</text>
	</string>

	<string id="ls_tip_13">
		<text>You need a detector to search for artefacts. The best detectors make searching easier and may even detect more valuable artefacts.</text>
	</string>

	<string id="ls_tip_14">
		<text>Stalkers don't like it when you point your weapon at them and may ask you to hide it if they feel threatened. You should holster your weapon before you talk to them.</text>
	</string>

	<string id="ls_tip_15">
		<text>You can locate the edges of an anomaly by throwing bolts. To get your bolt out, press "$$ACTION_WPN_6$$".</text>
	</string>

	<string id="ls_tip_16">
		<text>One of the most common threats in the Zone is radiation. Serious exposure will harm your health and can be terminal if not treated adequately.</text>
	</string>

	<string id="ls_tip_17">
		<text>As well as addressing your hunger, food will also slightly improve your health.</text>
	</string>

	<string id="ls_tip_18">
		<text>Energy drinks temporarily improve your endurance recovery, which increases your potential mobility.</text>
	</string>

	<string id="ls_tip_19">
		<text>Take some food with you when going out on a long raid. If you get really hungry, your endurance recovery will suffer greatly.</text>
	</string>

	<string id="ls_tip_20">
		<text>Low endurance can make you immobile and vulnerable to enemy attack at crucial moments.</text>
	</string>

	<string id="ls_tip_21">
		<text>You can monitor your concealment status using the noise and enemy detection indicators located in the top left-hand corner of the screen.</text>
	</string>

	<string id="ls_tip_22">
		<text>Having a detector in one hand doesn't stop you from using a knife, pistol or bolt with your other hand.</text>
	</string>

	<string id="ls_tip_23">
		<text>You can toggle certain data on or off via the buttons located at the top of your PDA.</text>
	</string>

	<string id="ls_tip_24">
		<text>The blood drop symbol in the bottom right-hand corner of the screen warns you of bleeding and the need to stop it. The colour of the symbol indicates how serious the bleeding is.</text>
	</string>

	<string id="ls_tip_25">
		<text>A radiation danger symbol in the bottom right-hand corner of the screen will warn you that you have been exposed to radiation. When this happens, you should use anti-radiation meds. The colour of the symbol indicates how badly exposed you are.</text>
	</string>

	<string id="ls_tip_26">
		<text>Your relations with people directly affect the price of goods and services you may wish to purchase. If your relationship is sour, don't expect a discount. If you're getting along well, the trader might even be inclined to offer you something special.</text>
	</string>

	<string id="ls_tip_27">
		<text>You can bring up the context menu for items in your backpack by placing your mouse over the required item and pressing the right mouse button.</text>
	</string>

	<string id="ls_tip_28">
		<text>In addition to their useful effects, most artefacts are also radioactive. This can be compensated for by using artefacts that absorb radiation.</text>
	</string>

	<string id="ls_tip_29">
		<text>Your knife is ineffective against enemies armed with firearms. Use it in close combat or for a stealthy kill.</text>
	</string>

	<string id="ls_tip_30">
		<text>A detector is the only reliable way of checking anomalous areas for artefacts.</text>
	</string>

	<string id="ls_tip_31">
		<text>Most artefacts will remain invisible as they move around in the anomalous area, until they are identified by your detector.</text>
	</string>

	<string id="ls_tip_32">
		<text>Mutants are more active at night, which explains why stalkers prefer to wait for sunlight before venturing into the Zone.</text>
	</string>

	<string id="ls_tip_33">
		<text>If you have suffered serious radiation exposure, use anti-radiation drugs. If you don't have any, use medkits to deal with the initial symptoms and until you make it to a medic.</text>
	</string>

	<string id="ls_tip_34">
		<text>Many stalkers believe that the legendary Wish Granter exists and is hidden somewhere within the Chernobyl NPP.</text>
	</string>

	<string id="ls_tip_35">
		<text>Selling artefacts isn't the only way to make some cash: offloading unneeded equipment can be a profitable business, too. Keep in mind that traders aren't usually interested in items that are seriously damaged.</text>
	</string>

	<string id="ls_tip_36">
		<text>To repair an item you'll need to find a technician and click the repair button once you have selected the item in the modification screen. You can also right-click on the item in the same window to select the appropriate action from the context menu.</text>
	</string>

	<string id="ls_tip_37">
		<text>Mechanics will require special equipment for certain upgrades. Returning this equipment to them will allow them to upgrade your weapons and equipment further.</text>
	</string>

	<string id="ls_tip_38">
		<text>To detach a silencer, scope or under-barrel grenade launcher from your weapon, right-click on the weapon in your backpack and select the appropriate action from the context menu.</text>
	</string>

	<string id="ls_tip_39">
		<text>Different factions have different relationships. Just because Duty are hostile to Freedom doesn't mean that Clear Sky and all other factions are too.</text>
	</string>

	<string id="ls_tip_40">
		<text>If you want to move with minimal noise, press "$$ACTION_ACCEL$$" to walk or "$$ACTION_CROUCH$$" to crouch.</text>
	</string>

	<string id="ls_tip_41">
		<text>Shotguns are close-combat weapons. The further your enemy is from you, the less effective they are.</text>
	</string>

	<string id="ls_tip_42">
		<text>When taking cover from small arms fire, make sure to hide behind something solid. Unlike concrete walls, wooden boards and sheet metal don't make for very safe cover.</text>
	</string>

	<string id="ls_tip_43">
		<text>Psi-storms and emissions will require you to take shelter. Standing out in the open during these events has dire consequences.</text>
	</string>

	<string id="ls_tip_44">
		<text>Bandages should be used immediately to prevent bleeding. Whilst bleeding you will gradually lose health and will end up having to use a medkit.</text>
	</string>

	<string id="ls_tip_45">
		<text>Each type of mutant in the Zone has its own combat traits. If you take these into account, you will be far more likely to survive.</text>
	</string>

	<string id="ls_tip_46">
		<text>Vodka, a cheap alternative to antirad meds, is the easiest way to reduce the effects of radiation on the body.</text>
	</string>

	<string id="ls_tip_47">
		<text>Enemies can and will use grenades. When you see the grenade indicator, leave the kill zone immediately.</text>
	</string>

	<string id="ls_tip_48">
		<text>Speeding up before you jump allows you to overcome big gaps and cracks.</text>
	</string>

	<string id="ls_tip_49">
		<text>Key indicators that show your health and endurance are located in the bottom right-hand corner of the screen.</text>
	</string>

	<string id="ls_tip_50">
		<text>Butterfly mines can be used to set ambushes for other stalkers or mutants. Just make sure not to stand next to them after placement as they will explode regardless of whether you are friend or foe.</text>
	</string>

	<string id="ls_tip_51">
		<text>Firing accuracy suffers considerably when you're on the move. Stand still or crouch to improve it.</text>
	</string>

	<string id="ls_tip_52">
		<text>Jobs are not offered exclusively by traders and mechanics. Ordinary members of your faction may have something for you, too.</text>
	</string>

	<string id="ls_tip_53">
		<text>The Zone has a life of its own, which means that there's always a chance of bumping into mutants or enemy stalkers in areas that you have already cleared.</text>
	</string>

	<string id="ls_tip_54">
		<text>Stay alert in the Zone and don't rely on old information. When you return to an anomalous area you've already explored, you may find that the old tested path is no longer available.</text>
	</string>

	<string id="ls_tip_55">
		<text>Artefacts are not usually static: they tend to move within an anomalous area. Gung-ho attempts to chase down an artefact will lead you into an anomaly in no time.</text>
	</string>

	<string id="ls_tip_56">
		<text>Some medical products have unique properties. For example, Hercules allows you to temporarily carry more weight, while psi-blocking drugs help you resist psi-attacks on your brain.</text>
	</string>

	<string id="ls_tip_57">
		<text>Some stalkers may offer you services, such as information, exclusive goods, or give you orders for specific artefacts.</text>
	</string>

	<string id="ls_tip_58">
		<text>Talking to regular stalkers may yield useful information about new areas or recent events.</text>
	</string>

	<string id="ls_tip_59">
		<text>New artefacts may appear in anomalous areas that you've already explored after an emission.</text>
	</string>

	<string id="ls_tip_60">
		<text>You can use any weapon in both weapon slots. This allows you to create flexible weapon combinations, such as assault rifle/shotgun and pistol/sniper rifle.</text>
	</string>

	<string id="ls_tip_61">
		<text>Every trader you encounter in various stalker camps offers his own, unique range of goods.  As an example, medics are bound to have the best selection of medical products.</text>
	</string>

	<string id="ls_tip_62">
		<text>If you get to a stalker camp wounded or with radiation poisoning, you can make use of assistance from the local medic.</text>
	</string>

	<string id="ls_tip_63">
		<text>Smash open any boxes and small metal containers you see. You may find food or medical items which you can then use or sell.</text>
	</string>

	<string id="ls_tip_64">
		<text>If you need to wait for a specific hour you can take a nap at the camp to pass the time. To do this, find the sleeping area, press "$$ACTION_USE$$" and choose how long you want to sleep for.</text>
	</string>

	<string id="ls_tip_65">
		<text>The night vision device in your helmet can be upgraded to the next-generation version. This will expand its coverage range considerably.</text>
	</string>

	<string id="ls_tip_66">
		<text>If you want to survive, always keep track of the crackling noise made by the Geiger counter, which warns you of radiation, and the anomaly danger signal that lets you know you're close to an anomaly.</text>
	</string>

	<string id="ls_tip_67">
		<text>When out in the Zone, keep some spare ammunition boxes on you - they're bound to come in handy.</text>
	</string>

	<string id="ls_tip_68">
		<text>Remember to save your game regularly - this will prevent you from playing the same parts over and over.</text>
	</string>

	<string id="ls_tip_69">
		<text>To use drugs appropriately it helps to know what they do. Consult drug descriptions to find out the effects of certain drugs.</text>
	</string>

	<string id="ls_tip_70">
		<text>Use grenades to attack enemies in cover. Keep in mind the grenade's blast radius to avoid hurting friendly fighters.</text>
	</string>

	<string id="ls_tip_71">
		<text>To discard an item from your backpack right-click the item and select the appropriate action from the context menu.</text>
	</string>

	<string id="ls_tip_72">
		<text>You can use the special setting in the options menu to have the game save your progress automatically at key points.</text>
	</string>

	<string id="ls_tip_73">
		<text>When you return to camp you should repair your equipment, sell unneeded loot and restock on ammo and medical supplies.</text>
	</string>

	<string id="ls_tip_74">
		<text>You can change the game difficulty in the game options menu at any time.</text>
	</string>

	<string id="ls_tip_75">
		<text>To reduce the harmful effects of anomalous areas during raids into the Zone, use a protective suit, artefacts, or take appropriate drugs.</text>
	</string>

	<string id="ls_tip_76">
		<text>Each weapon has unique accuracy, handling, damage and rate of fire characteristics, allowing you to pick one that's right for you from a large selection.</text>
	</string>

	<string id="ls_tip_77">
		<text>In addition to weapons and ammo, enemy bodies may contain valuable items like PDAs with information on them.</text>
	</string>

	<string id="ls_tip_78">
		<text>Artefacts aren't just valuable loot: in addition to being light and expensive, they can often have extremely useful effects.</text>
	</string>

	<string id="ls_tip_79">
		<text>Due to the Zone's environment, many mutants are radioactive. Eating meat you have field dressed from them without adequate radiation protection is extremely dangerous.</text>
	</string>

	<string id="ls_tip_80">
		<text>Some weapons allow you to install an adjustable long-range scope.  To adjust range, press "$$ACTION_NEXT_SLOT$$" or "$$ACTION_PREV_SLOT$$".</text>
	</string>

	<string id="ls_tip_81">
		<text>Most weapons have several firing modes by default and you can buy modifications to add new ones later. To switch between firing modes, press "$$ACTION_WPN_FIREMODE_NEXT$$" or "$$ACTION_WPN_FIREMODE_PREV$$".</text>
	</string>

	<string id="ls_tip_82">
		<text>Some places are impossible to pass through even when crouching. To crouch as low as you can, press and hold "$$ACTION_CROUCH$$" and "$$ACTION_ACCEL$$" simultaneously.</text>
	</string>

	<string id="ls_tip_83">
		<text>To take a screenshot, press "$$ACTION_SCREENSHOT$$".</text>
	</string>

	<string id="ls_tip_84">
		<text>To reduce your chances of being hit by enemy fire, return fire by leaning around corners. To do this hold "$$ACTION_LLOOKOUT$$" or "$$ACTION_RLOOKOUT$$".</text>
	</string>

	<string id="ls_tip_85">
		<text>Thanks to a built-in target location mechanism, your binoculars allow you to not only get a good look at distant objects but also locate enemies. To get your binoculars out, press "$$ACTION_WPN_5$$".</text>
	</string>

	<string id="ls_tip_86">
		<text>To turn your flashlight on or off, press "$$ACTION_TORCH$$".</text>
	</string>

	<string id="ls_tip_87">
		<text>To get your bolt out, press "$$ACTION_WPN_6$$".</text>
	</string>

	<string id="ls_tip_88">
		<text>If you have a night vision device built into your helmet, you can activate and deactivate it by pressing "$$ACTION_NIGHT_VISION$$".</text>
	</string>

	<string id="ls_tip_89">
		<text>Press "$$ACTION_QUICK_USE_1$$", "$$ACTION_QUICK_USE_2$$", "$$ACTION_QUICK_USE_3$$", "$$ACTION_QUICK_USE_4$$" to use the appropriate item in your quick access panel.</text>
	</string>

	<string id="ls_tip_90">
		<text>Your PDA has a map, information about current missions, your personal statistics and a message log. To open your PDA, press "$$ACTION_ACTIVE_JOBS$$".</text>
	</string>

	<string id="ls_tip_91">
		<text>You need to get your detector out to search for artefacts. To do this, press "$$ACTION_SHOW_DETECTOR$$".</text>
	</string>

	<string id="ls_tip_92">
		<text>To pause the game, press "$$ACTION_PAUSE$$".</text>
	</string>

	<string id="ls_tip_93">
		<text>To quicksave, press "$$ACTION_QUICK_SAVE$$". Press "$$ACTION_QUICK_LOAD$$" to quickload the last quicksave.</text>
	</string>

	<string id="ls_tip_94">
		<text>Running is the fastest way of moving, but also the most tiring. Press "$$ACTION_SPRINT_TOGGLE$$" to run.</text>
	</string>

	<string id="ls_tip_95">
		<text>Firing while aiming is far more accurate than firing from the hip. To aim, press "$$ACTION_WPN_ZOOM$$".</text>
	</string>

	<string id="ls_tip_96">
		<text>If you have an under-barrel grenade launcher, you can switch between it and the normal firing mode by pressing "$$ACTION_WPN_FUNC$$".</text>
	</string>

	<string id="ls_tip_97">
		<text>If you have several types of ammo for your current weapon, you can switch between them by pressing "$$ACTION_WPN_NEXT$$".</text>
	</string>

	<string id="ls_tip_98">
		<text>To reload your weapon before your magazine is empty, press "$$ACTION_WPN_RELOAD$$".</text>
	</string>

	<string id="ls_tip_99">
		<text>To discard your current weapon, press "$$ACTION_DROP$$".</text>
	</string>

	<string id="ls_tip_100">
		<text>To look in your backpack, press "$$ACTION_INVENTORY$$".</text>
	</string>

</string_table>
